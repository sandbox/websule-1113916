<?php
// $Id: druplus.install,v 0.9 2011/03/03 16:10:40 websule Exp $
/**
 * @file
 * Install, update and uninstall functions for the Druplus module.
 */

/**
 * Implementation of hook_schema().
 */
function druplus_schema() {
  // Table for packages and other products.
  $schema['druplus_apps'] = array(
    'description' => 'druplus_apps',
    'fields' => array(
      'appid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'appid',
        ),
      'appname' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'appname',
        'default' => ''
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'description',
        'default' => ''
      ),
       'appmod' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'appmod',
        'default' => ''
      ),
      'appcat' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'druplus app-group',
        'default' => 'druplus',
      ),
      'weight' => array(
        'type' => 'int',
        'length' => 'tiny',
        'not null' => TRUE,
        'description' => 'app-group order',
        'default' => 0,
      ),
    ),
    'primary key' => array('appid'),
  );


  $schema['druplus_packs'] = array(
    'description' => 'druplus_packs',
    'fields' => array(
      'modid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'modid',
      ),
      'appid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'appid',
      ),
      'title' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'name',
        'default' => '',
      ),
      'module' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'module',
        'default' => '',
      ),
      'download_url' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'download url',
        'default' => '',
      ),
      'path' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'enable',
        'default' => '',
      ),
      'settings' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'configure',
        'default' => '',
      ),
      'weight' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'weight',
        'default' => 0,
      ),
      'installed' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'UNIX timestamp of when this modules was installed, or 0 if the installed is not active.',
      ),
      'aid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {user}.uid of the account that installed this module.',
      ),
    ),
    'primary key' => array('modid'),
  );

  return $schema;

}


/**
 * Implementation druplus setsmart of custom functions().
 */
function druplus_setsmart(){

 // Admin Theme, set to seven if not set to anything else.
$admthm = variable_get('admin_theme', '');
if($admthm==""){variable_set('admin_theme', 'seven');}

 //cron configured to run every 6 hours.
 variable_set('cron_safe_threshold', '21600');

// Set cron last run time to db
  $timos = time(); // current time
  variable_set('cron_last', '$timos'); // set cron run to manual

// Admin Menu and stylying.
  if(!module_exists('admin_menu_toolbar')) {
  $toolbars = array('toolbar','shortcut');
  module_disable($toolbars);
  system_rebuild_module_data();
  $mods = array('admin_menu','admin_devel','admin_menu_toolbar');
  module_enable($mods);
  }
 variable_set('admin_menu_tweak_permissions', 1); // collapse sections for permissions page.
 variable_set('admin_menu_tweak_modules', 1); // collapse sections for modules page too.

  //Timezone configured Denver/US.
 $timzon=variable_get('date_default_timezone', '');
 if($timzon==''){
 variable_set('site_default_country', 'US');
 variable_set('date_default_timezone', 'America/Denver');
 variable_set('date_first_day', '1');
 variable_set('configurable_timezones', '0');
 }

 //Set update check notification.
 $update_checks = module_exists('update');
 if($update_checks=="1"){
 $admin_mail = variable_get('site_mail', 'webmaster@yoursite.com');
 $admin_mail = array($admin_mail);
 variable_set('update_notify_emails', $admin_mail);
 variable_set('update_check_frequency', '7');
 variable_set('update_notification_threshold', 'security');
 }

  //Set druplus key
  $base_url = "http://yoursite.com";
  $setkey = md5($base_url);
  variable_set('druplus_lic_key',$setkey);

}


/**
 * Implementation of hook_install().
*/
function druplus_install() {

// Checking and configuring Smart defaults to begin with
	druplus_setsmart();

  $t = get_t();

// Creating Druplus Apps Categories.
  db_query("INSERT INTO {druplus_apps} VALUES (1, 'CMS', 'Druplus CMS plugins & basic modules.', 'druplus_cms', 'druplus', '0')");
  db_query("INSERT INTO {druplus_apps} VALUES (2, 'Media', 'Druplus Multimedia Solution.', 'druplus_media', 'common', '1')");
  db_query("INSERT INTO {druplus_apps} VALUES (3, 'Blogger', 'Druplus blogging tools.', 'druplus_blogger', 'common', '2')");
  db_query("INSERT INTO {druplus_apps} VALUES (4, 'Forums', 'Druplus Forums setup & default configurations.', 'druplus_forums', 'common', '3')");
  db_query("INSERT INTO {druplus_apps} VALUES (5, 'Fieldsplus', 'Druplus Content Creation Kit pack with most widely used plugins.', 'druplus_cck', 'common', '4')");
  db_query("INSERT INTO {druplus_apps} VALUES (6, 'Views-pack', 'Druplus Views & Views Bulk operations combined together.', 'druplus_views', 'common', '5')");
  db_query("INSERT INTO {druplus_apps} VALUES (7, 'SEO-plugins', 'Powerful Druplus Search Engine Optimization tool-kit.', 'druplus_seo', 'common', '6')");
  db_query("INSERT INTO {druplus_apps} VALUES (8, 'Extras Addons', 'Powerful Drupalus plugins to enhance functionality.', '', 'common', '7')");


  // Lets populate modules for all the packs!
  $mod_ins = "(modid, appid, title, module, download_url, path, settings, weight)";
  
    // WYSWYG autoconfiguration settings
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (1, 1, 'WYSIWYG - (default is CKedit basic)', 'wysiwyg', 'http://drupal.org/project/wysiwyg', '', 'admin/settings/wysiwyg', 1)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (2, 1, 'Imce - (image management tools)', 'imce', 'http://drupal.org/project/imce', '', 'admin/settings/imce', 2)");
    
    // Captcha Settings
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (3, 1, 'Captcha - (automated spam protection)', 'captcha', 'http://drupal.org/project/captcha', '', 'admin/user/captcha/captcha', 3)");
  
    // Admin tools & settings
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (4, 1, 'Views Manager(Custom pages & blocks)', 'views', 'http://drupal.org/project/views', '', '', 4)");

    // Blog Family
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (5, 2, 'Blog - (powerful blogging solution)', 'blog', '', '', 'blog', 5)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (6, 2, 'Blog Categories', 'blog', '', '', 'admin/content/taxonomy', 6)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (7, 2, 'Blog Comments', 'blog', '', '', 'admin/content/node-type/blog', 7)");
  
   // Forum Family
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (8, 3, 'Forum - (enhanced forums)', 'forum', '', '', 'forum', 8)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (9, 3, 'Forum Categories', 'forum', '', '', 'admin/content/forum/list', 9)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (10, 3, 'Forum Comments', 'forum', '', '', 'admin/content/types/forum/edit', 10)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (11, 3, 'Advanced Forums', 'advanced_forum', 'http://drupal.org/project/advanced_forum', '', 'admin/content/node-type/forum', 12)");
  
   // Image Family
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (12, 4, 'Media (Images)', 'media', 'http://drupal.org/project/media', '', 'admin/settings/media', 19)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (13, 4, 'Media Gallery - (manage media)', 'media_gallery', 'http://drupal.org/project/media_gallery', 'image/contrib/', 'admin/settings/media_gallery', 13)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (14, 4, 'Media Youtube - (embed youtube)', 'media_youtube', 'http://drupal.org/project/media_youtube', '', 'admin/settings/media_youtube', 14)");
  
   // CCK Family
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (15, 5, 'Content construction Kit - (custom fields)', 'field_ui', 'http://drupal.org/project/drupal', '', 'admin/structure/types', 15)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (16, 5, 'AutoComplete Widgets - (ajaxified cck field)', 'autocomplete_widgets', 'http://drupal.org/project/autocomplete_widgets', '', 'admin/content/types/fields', 16)");
  
   // SEO Family
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (17, 7, 'Clean URLs - (Usually automatic. Please double-check!)', '', '', '', 'admin/settings/clean-urls', 17)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (18, 7, 'Pathauto - (for custom patterns in urls)', 'pathauto', 'http://drupal.org/project/pathauto', '', 'admin/build/path/patterns', 18)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (19, 7, 'Global Redirect - (avoids duplicate urls)', 'globalredirect', 'http://drupal.org/project/globalredirect', '', 'admin/settings/globalredirect', 19)");


   // Extras Addons
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (20, 8, 'Contact Forms - (contact forms with auto-messaging)', 'contact', '', '', 'admin/build/contact', 20)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (21, 8, 'Backup Migrate - (automatic & manual backups)', 'backup_migrate', 'http://drupal.org/project/backup_migrate', '', 'admin/content/backup_migrate', 21)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (22, 8, 'Google Analytics - (track your visitors)', 'google_analytics', 'http://drupal.org/project/google_analytics', '', 'admin/settings/googleanalytics', 22)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (23, 8, 'Webforms - (custom forms toolkit)', 'webform', 'http://drupal.org/project/webform', '', 'admin/settings/webform', 23)");
  db_query("INSERT INTO {druplus_packs} $mod_ins VALUES (24, 1, 'Admin Menu (enhances admin tasks)', 'admin_menu', 'http://drupal.org/project/admin_menu', '', '', 24)");



// Check if it is during installation profile using install check variable.
  if (variable_get('install_time', 0) != '') {
 // Being friendly to your users: what to do after install?
  drupal_set_message($t('You can now <a href="!druplus_admin">configure druplus apps</a> for your site.',
    array('!druplus_admin' => url('admin/druplus/apps'))), 'status');
   }
   else{
   drupal_set_message('');
   }

 
}


/**
 * Implementation of hook_uninstall().
 */
function druplus_uninstall() {
 db_query("DELETE FROM {variable} WHERE name LIKE 'druplus_%'");
 drupal_flush_all_caches('variable', 'druplus');
 drupal_uninstall_schema('druplus');
}
