<?php
// $Id: druplus.master.inc,v 1.0 $

/**
 * @file
 * Administrative page callbacks for the druplus module.
 */

define('druplus_websule_affiliate', 'http://www.websule.com/affiliate/$webfid');

/**
 * Define the settings form.
 */
function druplus_master_settings() {
  global $user;

/*
  $form['save_above'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
*/

  // Lets get all packs and modules from database.
  $query = "SELECT appid, appname, description, appmod FROM {druplus_apps} where appcat='druplus' or appcat='common' ORDER BY weight";
  $result = db_query($query);
  $group_id = 0;

  // packages are set to show up as fieldsets.
  foreach ($result as $data) {
    $group_id = intval($data->appid);
    
//modules install page link.
global $base_path;
$mod_page = $base_path."admin/build/modules";

// lets check if the master module is enabled or not! then take the action accordingly.
$pmod = $data->appmod;

if($pmod!=''){
$pmod_check = module_exists($pmod);
if($pmod_check==""){
$settin = "<span class='master_disabled'>$data->appmod is disabled, <a href='$mod_page' title='Click here to install $data->appmod'>install now</a> to enable all listed features.</span>";
}
else{
$settin = "<span class='master_enabled'>$data->pack is already enabled & running properly.</span>";
}
}
else{
 $settin = "<span class='addons'>$data->description</span>"; 
}
  $desc = $data->description;
    $form['group_' . $group_id] = array(
      '#type' => 'fieldset',
      '#title' => t($data->appmod),
      '#collapsible' => TRUE,
      '#description' => "$settin",
      '#group' => 'druplus',
    );
    
    $res = db_query("SELECT download_url, path, settings, module, installed, modid, title, aid FROM {druplus_packs} WHERE appid = '$group_id' ORDER BY weight");
    foreach ($res as $row) {
      $row->links = array();

      //if ($row->installed) {
      $modname=$row->module;
      if($modname=="google_analytics"){$modname="googleanalytics";} // special case for google analytics module.
      if(!$row->module || module_exists($modname)){
        $classic = "dp_enabled";

        // Show when and who installed this pack.
        $row->aid = '1'; // admin id - needs to be fixed!

        //$row->links[] = t('installed on %date by !username', array('%date' => format_date($row->installed, 'small'), '!username' => theme('username', user_load($row->aid))));
      }
      else {
        // Show non-installed sub-tasks.
        $modname = $row->module;
        if($modname=="content"){$modname="cck";} // special case for cck aka content module.
        $modpath = $row->path;
        $directori = "modules/$modpath$modname";
        $directoris = "sites/all/modules/$modpath$modname";
        $checkinn = file_prepare_directory(&$directori, $mode = 0, $form_item = NULL);
        $checkout = file_prepare_directory(&$directoris, $mode = 0, $form_item = NULL);
        if ($checkinn=="" && $checkout=="") {
        if ($row->download_url) {
          $row->links[] = l(t('Download'), $row->download_url, array('attributes' => array('target' => '_blank')));
          $classic = "dp_missing";
        }
       }
        else{
          $mods_link = "admin/build/modules";
          $row->links[] = l(t('Enable'), $mods_link);
          $classic = "dp_disabled";
        }
        
      }

      $modname=$row->module;
      if($modname=="google_analytics"){$modname="googleanalytics";} // special case for google analytics module.
       if ($row->settings && (!$row->module || module_exists($modname))) {
        // Show the link to configure if this isn't a module or module is enabled.
        $classic = "dp_enabled";
        $row->links[] = l(t('Configure'), $row->settings);
      }
      
        $task = $form['group_' . $group_id]['w3bsule_task_' . $row->modid] = array(
        '#type' => 'checkbox',
        '#title' => t($row->title),
        '#default_value' => $row->installed || ($row->module && module_exists($row->module)),
        '#description' => join(' | ', $row->links),
        '#attributes' => array('class' => $classic),
        '#prefix' => "<span class=$classic>",
        '#suffix' => '</span>',
      );
    }
    
  }

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );


  return $form;
}